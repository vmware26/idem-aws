"""
Autogenerated using `pop-create-idem <https://gitlab.com/saltstack/pop/pop-create-idem>`__

hub.exec.boto3.client.rds.backtrack_db_cluster
hub.exec.boto3.client.rds.create_db_cluster
hub.exec.boto3.client.rds.delete_db_cluster
hub.exec.boto3.client.rds.describe_db_clusters
hub.exec.boto3.client.rds.failover_db_cluster
hub.exec.boto3.client.rds.modify_db_cluster
hub.exec.boto3.client.rds.reboot_db_cluster
hub.exec.boto3.client.rds.start_db_cluster
hub.exec.boto3.client.rds.stop_db_cluster
"""
import copy
from collections import OrderedDict
from typing import Any
from typing import Dict
from typing import List

__contracts__ = ["resource"]
create_waiter_acceptors = [
    {
        "matcher": "pathAll",
        "expected": "available",
        "state": "success",
        "argument": "DBClusters[].Status",
    },
    {
        "matcher": "pathAll",
        "expected": "creating",
        "state": "retry",
        "argument": "DBClusters[].Status",
    },
]
update_waiter_acceptors = [
    {
        "matcher": "pathAll",
        "expected": "available",
        "state": "success",
        "argument": "DBClusters[].Status",
    },
    {
        "matcher": "pathAll",
        "expected": "modifying",
        "state": "retry",
        "argument": "DBClusters[].Status",
    },
    {
        "matcher": "pathAll",
        "expected": "resetting-master-credentials",
        "state": "retry",
        "argument": "DBClusters[].Status",
    },
    {
        "matcher": "pathAll",
        "expected": "renaming",
        "state": "retry",
        "argument": "DBClusters[].Status",
    },
    {
        "matcher": "pathAll",
        "expected": "upgrading",
        "state": "retry",
        "argument": "DBClusters[].Status",
    },
]
delete_waiter_acceptors = [
    {
        "matcher": "error",
        "expected": "DBClusterNotFoundFault",
        "state": "success",
        "argument": "Error.Code",
    },
    {
        "matcher": "pathAll",
        "expected": "available",
        "state": "retry",
        "argument": "DBClusters[].Status",
    },
    {
        "matcher": "pathAll",
        "expected": "deleting",
        "state": "retry",
        "argument": "DBClusters[].Status",
    },
]

TREQ = {
    "present": {
        "require": ["aws.rds.db_subnet_group.present"],
    },
}


async def present(
    hub,
    ctx,
    name: str,
    engine: str,
    resource_id: str = None,
    availability_zones: List = None,
    backup_retention_period: int = None,
    character_set_name: str = None,
    database_name: str = None,
    db_cluster_parameter_group_name: str = None,
    vpc_security_group_ids: List = None,
    db_subnet_group_name: str = None,
    engine_version: str = None,
    port: int = None,
    master_username: str = None,
    master_user_password: str = None,
    option_group_name: str = None,
    preferred_backup_window: str = None,
    preferred_maintenance_window: str = None,
    replication_source_identifier: str = None,
    tags: List[Dict[str, Any]] or Dict[str, Any] = None,
    storage_encrypted: bool = None,
    kms_key_id: str = None,
    pre_signed_url: str = None,
    enable_iam_database_authentication: bool = None,
    backtrack_window: int = None,
    enable_cloudwatch_logs_exports: List = None,
    engine_mode: str = None,
    scaling_configuration: Dict = None,
    deletion_protection: bool = None,
    global_cluster_identifier: str = None,
    enable_http_endpoint: bool = None,
    copy_tags_to_snapshot: bool = None,
    domain: str = None,
    domain_iam_role_name: str = None,
    enable_global_write_forwarding: bool = None,
    db_cluster_instance_class: str = None,
    allocated_storage: int = None,
    storage_type: str = None,
    iops: int = None,
    publicly_accessible: bool = None,
    auto_minor_version_upgrade: bool = None,
    monitoring_interval: int = None,
    monitoring_role_arn: str = None,
    enable_performance_insights: bool = None,
    performance_insights_kms_key_id: str = None,
    performance_insights_retention_period: int = None,
    source_region: str = None,
    apply_immediately: bool = None,
    allow_major_version_upgrades: bool = None,
    cloudwatch_logs_export_configuration: Dict = None,
    db_instance_parameter_group_name: str = None,
    timeout: Dict = None,
) -> Dict[str, Any]:
    r'''
    **Autogenerated function**

    Creates a new Amazon Aurora DB cluster or Multi-AZ DB cluster. You can use the ReplicationSourceIdentifier
    parameter to create an Amazon Aurora DB cluster as a read replica of another DB cluster or Amazon RDS MySQL or
    PostgreSQL DB instance. For cross-Region replication where the DB cluster identified by
    ReplicationSourceIdentifier is encrypted, also specify the PreSignedUrl parameter. For more information on
    Amazon Aurora, see  What is Amazon Aurora? in the Amazon Aurora User Guide.  For more information on Multi-AZ DB
    clusters, see  Multi-AZ deployments with two readable standby DB instances in the Amazon RDS User Guide.   The
    Multi-AZ DB clusters feature is in preview and is subject to change.

    Args:
        hub:
        ctx:
        name(Text): A name, ID to identify the resource.
        engine(Text): Name of the database engine to be used for this DB cluster
        resource_id(Text, optional): AWS Id of the resource
        availability_zones(List, optional): A list of Availability Zones (AZs) where DB instances in the DB cluster can
                                            be created.
        backup_retention_period(int, optional): The number of days for which automated backups are retained.
        character_set_name(Text, optional): A value that indicates the CharacterSet that should be associated with
                                           db cluster
        database_name(Text, optional): The name for your database of up to 64 alphanumeric characters
        db_cluster_parameter_group_name(Text, optional): Value to indicate the DB cluster parameter group to be
                                                        associated with dbcluster
        vpc_security_group_ids(list, optional): A list of EC2 VPC security groups to associate with this DB cluster
        db_subnet_group_name(Text, optional): DB subnet group name
        engine_version(Text, optional): Version number of the database engine to use
        port(int, optional): The port number on which the instances in the DB cluster accept connections.
        master_username(Text, optional): Name of the master user for the DB cluster.
        master_user_password(Text, optional): Password for the master user.This password can contain any printable
                                             ASCII character except "/", """, or "@".
        option_group_name(Text, optional):A value that indicates that the DB cluster should be associated with the
                                         specified option group.
                                         DB clusters are associated with a default option group that can't be modified.
        preferred_backup_window(Text, optional): The daily time range during which automated backups are created if
                                                automated backups are enabled using the BackupRetentionPeriod parameter.
        preferred_maintenance_window(Text, optional): The weekly time range during which system maintenance can occur,
                                                     in Universal Coordinated Time (UTC).
        replication_source_identifier(Text, optional): The Amazon Resource Name (ARN) of the source DB instance or
                                                      DB cluster if this DB cluster is created as a read replica.
        tags(List or Dict, optional): Tags to assign to the DB cluster. List of tags in the format of [{"Key": tag-key, "Value": tag-value}] or dict in the format of
                                      {tag-key: tag-value}
        storage_encrypted(bool, optional): Value that indicates whether the DB cluster is encrypted
        kms_key_id(Text, optional): The Amazon Web Services KMS key identifier for an encrypted DB cluster.
        pre_signed_url(Text, optional): A URL that contains a Signature Version 4 signed request for the CreateDBCluster
                                       action to be called in the source Amazon Web Services Region where the DB cluster
                                       is replicated from.Specify PreSignedUrl only when you are performing cross-Region
                                       replication from an encrypted DB cluster.
                                       The pre-signed URL must be a valid request for the CreateDBCluster API action
                                       that can be executed in the source Amazon Web Services Region that contains the
                                       encrypted DB cluster to be copied.
        enable_iam_database_authentication(bool, optional): A value that indicates whether to enable mapping of Amazon
                                                            Web Services.
                                                            Identity and Access Management (IAM) accounts to database
                                                            accounts.By default, mapping isn't enabled.
        backtrack_window(int, optional): The target backtrack window, in seconds. To disable backtracking, set this
                                        value to 0.
        enable_cloudwatch_logs_exports(list, optional): The list of log types that need to be enabled for exporting to
                                                        CloudWatch Logs.
        engine_mode(Text, optional): The DB engine mode of the DB cluster, either provisioned , serverless ,
                                    parallelquery , global or multimaster .
        scaling_configuration(Dict, optional): Used to specify scaling properties for the DB clusters in serverless DB
                                               engine mode
        deletion_protection(bool, optional): A value to indicate whether the DB cluster has deletion protection enabled.
                                             The database can't be deleted when deletion protection is enabled.
        global_cluster_identifier(Text, optional): The global cluster ID of an Aurora cluster that becomes the primary
                                                  cluster in the new global database cluster.
        enable_http_endpoint(bool, optional): A value to indicates whether to enable the HTTP endpoint for an Aurora
                                              Serverless DB cluster. By default, the HTTP endpoint is disabled.
        copy_tags_to_snapshot(bool, optional): A value that indicates whether to copy all tags from the DB cluster to
                                               snapshots of the DB cluster. The default is not to copy them.
        domain(Text, optional): The Active Directory, directory ID to create the DB cluster in.
        domain_iam_role_name(Text, optional): Specify the name of the IAM role to be used when making API calls to the
                                             Directory Service.
        enable_global_write_forwarding(bool, optional): A value that indicates whether to enable this DB cluster to
                                                        forward write operations to the primary cluster of an Aurora
                                                        global database ( GlobalCluster ).
        db_cluster_instance_class(Text, optional): Value to indicate compute and memory capacity of each DB instance in
                                                  the Multi-AZ DB cluster, for example db.m6g.xlarge.Not all DB instance
                                                  classes are available in all Amazon Web Services Regions, or for all
                                                  database engines.
        allocated_storage(int, optional): The amount of storage in gibibytes (GiB) to allocate to each DB instance in
                                          the Multi-AZ DB cluster.
        storage_type(Text, optional): Specifies the storage type to be associated with the DB cluster.
        iops(int, optional): The amount of Provisioned IOPS (input/output operations per second) to be initially
                             allocated for each DB instance in the Multi-AZ DB cluster.
        publicly_accessible(bool, optional): A value to indicates whether the DB cluster is publicly accessible.
        auto_minor_version_upgrade(bool, optional): A value to indicates whether minor engine upgrades are applied
                                                    automatically to the DB cluster during the maintenance window.
                                                    By default, minor engine upgrades are applied automatically.
        monitoring_interval(int, optional): The interval, in seconds, between points when Enhanced Monitoring metrics
                                            are collected for the DB cluster. To turn off collecting Enhanced Monitoring
                                            metrics, specify 0. The default is 0.
                             If MonitoringRoleArn is specified, also set MonitoringInterval to a value other than 0.
        monitoring_role_arn(Text, optional): The Amazon Resource Name (ARN) for the IAM role that permits RDS to send
                                            Enhanced Monitoring metrics to Amazon CloudWatch Logs
        enable_performance_insights(bool, optional): A value that indicates whether to turn on Performance Insights for
                                                     the DB cluster.
        performance_insights_kms_key_id(Text, optional): The Amazon Web Services KMS key identifier for encryption of
                                                        Performance Insights data.
                                                        The Amazon Web Services KMS key identifier is the key ARN,
                                                        key ID, alias ARN, or alias name for the KMS key.
                                                        If you don't specify a value for PerformanceInsightsKMSKeyId ,
                                                        then Amazon RDS uses your default KMS key.
        performance_insights_retention_period(int, optional): The amount of time, in days, to retain Performance
                                                              Insights data.
        source_region(Text, optional): The ID of the region that contains the source for the db cluster.
        apply_immediately(bool, optional): A value that indicates whether the modifications in this request and any
                                           pending modifications are asynchronously applied as soon as possible,
                                           regardless of the PreferredMaintenanceWindow setting for the DB cluster.
                                           If this parameter is disabled, changes to the DB cluster are applied during
                                           the next maintenance window.
                                           The ApplyImmediately parameter only affects the
                                           EnableIAMDatabaseAuthentication,MasterUserPassword and NewDBClusterIdentifier
                                           values.If the ApplyImmediately parameter is disabled, then changes to the
                                           EnableIAMDatabaseAuthentication,MasterUserPassword and NewDBClusterIdentifier
                                           values are applied during the next maintenance window. All other changes are
                                           applied immediately,regardless of the value of the ApplyImmediately parameter
        allow_major_version_upgrades(bool, optional): A value that indicates whether major version upgrades are allowed.
        cloudwatch_logs_export_configuration(Dict, optional): The configuration setting for the log types to be enabled
                                                              for export to CloudWatch Logs for a specific DB cluster.
        db_instance_parameter_group_name(Text, optional): The name of the DB parameter group to apply to all instances of
                                                         the DB cluster.
        timeout(Dict, optional): Timeout configuration for create/update of AWS DB Cluster.
            * create (Dict) -- Timeout configuration for creating DB Cluster
                * delay -- The amount of time in seconds to wait between attempts.
                * max_attempts -- Customized timeout configuration containing delay and max attempts.
            * update (Dict) -- Timeout configuration for updating DB Cluster
                * delay -- The amount of time in seconds to wait between attempts.
                * max_attempts -- Customized timeout configuration containing delay and max attempts.

    Request Syntax:
        [db-cluster-name]:
          aws.rds.db_cluster.present:
           - availability_zone:'List'
           - backup_retention_period: 'int'
           - database_name: 'string'
           - engine: 'string'
           - engine_version: 'string'
           - port: 'int'
           - master_username: 'string'
           - master_user_password: 'string'
           - preferred_backup_window: 'string'
           - preferred_maintenance_window: 'string'
           - storage_encrypted: 'bool'
           - kms_key_id: 'string'
           - engine_mode: 'string'
           - deletion_protection: 'bool'
           - copy_tags_to_snapshot: 'bool'
           - vpc_security_group_ids: 'List'
           - db_cluster_parameter_group_name: 'string'
           - tags: 'List'


    Returns:
        Dict[str, Any]

    Examples:

        .. code-block:: sls

            db-cluster-1:
              aws.rds.db_cluster.present:
               - availability_zone:
                    - us-east-2b
                    - us-east-2c
                    - us-east-2a
               - backup_retention_period: 7
               - database_name: dbname123
               - engine: aurora-postgresql
               - engine_version: '12.7'
               - port: 5432
               - master_username: postgres
               - master_user_password: abcd1234
               - preferred_backup_window: 07:41-08:11
               - preferred_maintenance_window: sat:09:29-sat:09:59
               - storage_encrypted: true
               - kms_key_id: arn:aws:kms:us-east-2:537227425989:key/e9e79921-8dda-48d7-afd7-38a64dd8e9b1
               - engine_mode: provisioned
               - deletion_protection: false
               - copy_tags_to_snapshot: true
               - vpc_security_group_ids:
                 - sg-f5eeba9c
               - db_cluster_parameter_group_name: default.aurora-postgresql12
               - tags:
                 - Key: name
                   Value: value
    '''

    result = dict(comment=(), old_state="", new_state="", name=name, result=True)
    params_to_modify = {}
    before = None
    plan_state = None
    resource_updated = False
    if resource_id:
        before = await hub.exec.boto3.client.rds.describe_db_clusters(
            ctx, DBClusterIdentifier=resource_id
        )
    tags = (
        hub.tool.aws.tag_utils.convert_tag_list_to_dict(tags)
        if isinstance(tags, List)
        else tags
    )
    if before and before.get("ret"):
        try:
            result[
                "old_state"
            ] = hub.tool.aws.rds.conversion_utils.convert_raw_db_cluster_to_present(
                raw_resource=before["ret"]["DBClusters"][0], idem_resource_name=name
            )
            plan_state = copy.deepcopy(result["old_state"])
            resource_arn = before["ret"]["DBClusters"][0].get("DBClusterArn")
            old_tags = result["old_state"].get("tags")
            if tags is not None:
                update_tags_ret = await hub.exec.aws.rds.tag.update_rds_tags(
                    ctx,
                    resource_arn=resource_arn,
                    old_tags=old_tags,
                    new_tags=tags,
                )
                if not update_tags_ret["result"]:
                    result["comment"] = update_tags_ret["comment"]
                    result["result"] = False
                    return result
                resource_updated = update_tags_ret["result"]
                result["comment"] = update_tags_ret["comment"]
                if ctx.get("test", False) and update_tags_ret["result"]:
                    plan_state["tags"] = update_tags_ret["ret"]

            modify_params = OrderedDict(
                {
                    "ApplyImmediately": "apply_immediately",
                    "BackupRetentionPeriod": "backup_retention_period",
                    "DBClusterParameterGroupName": "db_cluster_parameter_group_name",
                    "VpcSecurityGroupIds": "vpc_security_group_ids",
                    "Port": "port",
                    "MasterUserPassword": "master_user_password",
                    "OptionGroupName": "option_group_name",
                    "PreferredBackupWindow": "preferred_backup_window",
                    "PreferredMaintenanceWindow": "preferred_maintenance_window",
                    "EnableIAMDatabaseAuthentication": "enable_iam_database_authentication",
                    "BacktrackWindow": "backtrack_window",
                    "CloudwatchLogsExportConfiguration": "cloudwatch_logs_export_configuration",
                    "EngineVersion": "engine_version",
                    "AllowMajorVersionUpgrade": "allow_major_version_upgrades",
                    "DBInstanceParameterGroupName": "db_instance_parameter_group_name",
                    "Domain": "domain",
                    "DomainIAMRoleName": "domain_iam_role_name",
                    "ScalingConfiguration": "scaling_configuration",
                    "DeletionProtection": "deletion_protection",
                    "EnableHttpEndpoint": "enable_http_endpoint",
                    "CopyTagsToSnapshot": "copy_tags_to_snapshot",
                    "EnableGlobalWriteForwarding": "enable_global_write_forwarding",
                    "DBClusterInstanceClass": "db_cluster_instance_class",
                    "AllocatedStorage": "allocated_storage",
                    "StorageType": "storage_type",
                    "Iops": "iops",
                    "AutoMinorVersionUpgrade": "auto_minor_version_upgrade",
                    "MonitoringInterval": "monitoring_interval",
                    "MonitoringRoleArn": "monitoring_role_arn",
                    "EnablePerformanceInsights": "enable_performance_insights",
                    "PerformanceInsightsKMSKeyId": "performance_insights_kms_key_id",
                    "PerformanceInsightsRetentionPeriod": "performance_insights_retention_period",
                }
            )
            for parameter_raw, parameter_present in modify_params.items():
                # Add to modify list only if parameter is changed
                if (
                    locals()[parameter_present] is not None
                    and result["old_state"].get(parameter_present)
                    != locals()[parameter_present]
                ):
                    params_to_modify[parameter_raw] = locals()[parameter_present]
            if params_to_modify:
                if ctx.get("test", False):
                    result["comment"] = result["comment"] + (
                        f"Would update aws.rds.db_cluster '{name}'",
                    )
                    for key, value in params_to_modify.items():
                        plan_state[modify_params.get(key)] = value
                else:
                    modify_ret = await hub.exec.boto3.client.rds.modify_db_cluster(
                        ctx, DBClusterIdentifier=resource_id, **params_to_modify
                    )
                    if not modify_ret["result"]:
                        result["comment"] = result["comment"] + modify_ret["comment"]
                        result["result"] = False
                        return result
                    resource_updated = resource_updated or modify_ret["result"]

                    # Custom Waiter for update
                    waiter_config = hub.tool.aws.waiter_utils.create_waiter_config(
                        default_delay=15,
                        default_max_attempts=40,
                        timeout_config=timeout.get("update") if timeout else None,
                    )
                    cluster_waiter = hub.tool.boto3.custom_waiter.waiter_wrapper(
                        name="ClusterModified",
                        operation="DescribeDBClusters",
                        argument=["DBClusters[].Status"],
                        acceptors=update_waiter_acceptors,
                        client=hub.tool.boto3.client.get_client(ctx, "rds"),
                    )
                    await hub.tool.boto3.client.wait(
                        ctx,
                        "rds",
                        "ClusterModified",
                        cluster_waiter,
                        30,
                        DBClusterIdentifier=resource_id,
                        WaiterConfig=waiter_config,
                    )

        except hub.tool.boto3.exception.ClientError as e:
            result["comment"] = result["comment"] + (f"{e.__class__.__name__}: {e}",)
            result["result"] = False

    else:
        if ctx.get("test", False):
            result["new_state"] = hub.tool.aws.test_state_utils.generate_test_state(
                enforced_state={},
                desired_state={
                    "name": name,
                    "availability_zones": availability_zones,
                    "backup_retention_period": backup_retention_period,
                    "character_set_name": character_set_name,
                    "database_name": database_name,
                    "db_cluster_parameter_group_name": db_cluster_parameter_group_name,
                    "vpc_security_group_ids": vpc_security_group_ids,
                    "db_subnet_group_name": db_subnet_group_name,
                    "engine": engine,
                    "engine_version": engine_version,
                    "port": port,
                    "master_username": master_username,
                    "master_user_password": master_user_password,
                    "option_group_name": option_group_name,
                    "preferred_backup_window": preferred_backup_window,
                    "preferred_maintenance_window": preferred_maintenance_window,
                    "replication_source_identifier": replication_source_identifier,
                    "tags": tags,
                    "storage_encrypted": storage_encrypted,
                    "kms_key_id": kms_key_id,
                    "pre_signed_url": pre_signed_url,
                    "enable_iam_database_authentication": enable_iam_database_authentication,
                    "backtrack_window": backtrack_window,
                    "enable_cloudwatch_logs_exports": enable_cloudwatch_logs_exports,
                    "engine_mode": engine_mode,
                    "scaling_configuration": scaling_configuration,
                    "deletion_protection": deletion_protection,
                    "global_cluster_identifier": global_cluster_identifier,
                    "enable_http_endpoint": enable_http_endpoint,
                    "copy_tags_to_snapshot": copy_tags_to_snapshot,
                    "domain": domain,
                    "domain_iam_role_name": domain_iam_role_name,
                    "enable_global_write_forwarding": enable_global_write_forwarding,
                    "db_cluster_instance_class": db_cluster_instance_class,
                    "allocated_storage": allocated_storage,
                    "storage_type": storage_type,
                    "iops": iops,
                    "publicly_accessible": publicly_accessible,
                    "auto_minor_version_upgrade": auto_minor_version_upgrade,
                    "monitoring_interval": monitoring_interval,
                    "monitoring_role_arn": monitoring_role_arn,
                    "enable_performance_insights": enable_performance_insights,
                    "performance_insights_kms_key_id": performance_insights_kms_key_id,
                    "performance_insights_retention_period": performance_insights_retention_period,
                    "source_region": source_region,
                },
            )
            result["comment"] = (f"Would create aws.rds.db_cluster '{name}'",)
            return result
        try:
            ret = await hub.exec.boto3.client.rds.create_db_cluster(
                ctx,
                DBClusterIdentifier=name,
                AvailabilityZones=availability_zones,
                BackupRetentionPeriod=backup_retention_period,
                CharacterSetName=character_set_name,
                DatabaseName=database_name,
                DBClusterParameterGroupName=db_cluster_parameter_group_name,
                VpcSecurityGroupIds=vpc_security_group_ids,
                DBSubnetGroupName=db_subnet_group_name,
                Engine=engine,
                EngineVersion=engine_version,
                Port=port,
                MasterUsername=master_username,
                MasterUserPassword=master_user_password,
                OptionGroupName=option_group_name,
                PreferredBackupWindow=preferred_backup_window,
                PreferredMaintenanceWindow=preferred_maintenance_window,
                ReplicationSourceIdentifier=replication_source_identifier,
                Tags=hub.tool.aws.tag_utils.convert_tag_dict_to_list(tags)
                if tags
                else None,
                StorageEncrypted=storage_encrypted,
                KmsKeyId=kms_key_id,
                PreSignedUrl=pre_signed_url,
                EnableIAMDatabaseAuthentication=enable_iam_database_authentication,
                BacktrackWindow=backtrack_window,
                EnableCloudwatchLogsExports=enable_cloudwatch_logs_exports,
                EngineMode=engine_mode,
                ScalingConfiguration=scaling_configuration,
                DeletionProtection=deletion_protection,
                GlobalClusterIdentifier=global_cluster_identifier,
                EnableHttpEndpoint=enable_http_endpoint,
                CopyTagsToSnapshot=copy_tags_to_snapshot,
                Domain=domain,
                DomainIAMRoleName=domain_iam_role_name,
                EnableGlobalWriteForwarding=enable_global_write_forwarding,
                DBClusterInstanceClass=db_cluster_instance_class,
                AllocatedStorage=allocated_storage,
                StorageType=storage_type,
                Iops=iops,
                PubliclyAccessible=publicly_accessible,
                AutoMinorVersionUpgrade=auto_minor_version_upgrade,
                MonitoringInterval=monitoring_interval,
                MonitoringRoleArn=monitoring_role_arn,
                EnablePerformanceInsights=enable_performance_insights,
                PerformanceInsightsKMSKeyId=performance_insights_kms_key_id,
                PerformanceInsightsRetentionPeriod=performance_insights_retention_period,
                SourceRegion=source_region,
            )
            result["result"] = ret["result"]
            if not result["result"]:
                result["comment"] = ret["comment"]
                return result
            resource_id = ret["ret"]["DBCluster"]["DBClusterIdentifier"]

            # Custom waiter for create
            waiter_config = hub.tool.aws.waiter_utils.create_waiter_config(
                default_delay=15,
                default_max_attempts=40,
                timeout_config=timeout.get("create") if timeout else None,
            )
            cluster_waiter = hub.tool.boto3.custom_waiter.waiter_wrapper(
                name="ClusterCreated",
                operation="DescribeDBClusters",
                argument=["DBClusters[].Status"],
                acceptors=create_waiter_acceptors,
                client=hub.tool.boto3.client.get_client(ctx, "rds"),
            )
            await hub.tool.boto3.client.wait(
                ctx,
                "rds",
                "ClusterCreated",
                cluster_waiter,
                DBClusterIdentifier=resource_id,
                WaiterConfig=waiter_config,
            )
            result["comment"] = f"Created aws.rds.db_cluster '{name}'"

        except hub.tool.boto3.exception.ClientError as e:
            result["comment"] = result["comment"] + (f"{e.__class__.__name__}: {e}",)
            result["result"] = False

    try:
        if ctx.get("test", False):
            result["new_state"] = plan_state
        elif (not before) or resource_updated:
            after = await hub.exec.boto3.client.rds.describe_db_clusters(
                ctx,
                DBClusterIdentifier=resource_id,
            )
            result[
                "new_state"
            ] = hub.tool.aws.rds.conversion_utils.convert_raw_db_cluster_to_present(
                raw_resource=after["ret"]["DBClusters"][0], idem_resource_name=name
            )
        else:
            result["new_state"] = copy.deepcopy(result["old_state"])
    except Exception as e:
        result["comment"] = result["comment"] + (str(e),)
        result["result"] = False
    return result


async def absent(
    hub,
    ctx,
    name: str,
    resource_id: str,
    skip_final_snapshot: bool = None,
    final_db_snapshot_identifier: str = None,
    timeout: Dict = None,
) -> Dict[str, Any]:
    r"""
    **Autogenerated function**

    The DeleteDBCluster action deletes a previously provisioned DB cluster. When you delete a DB cluster, all
    automated backups for that DB cluster are deleted and can't be recovered. Manual DB cluster snapshots of the
    specified DB cluster are not deleted. For more information on Amazon Aurora, see  What is Amazon Aurora? in the
    Amazon Aurora User Guide.  For more information on Multi-AZ DB clusters, see  Multi-AZ deployments with two
    readable standby DB instances in the Amazon RDS User Guide.   The Multi-AZ DB clusters feature is in preview and
    is subject to change.

    Args:
        hub:
        ctx:
        name(Text): Idem name to identify the resource.
        resource_id(Text): AWS ID to identify the resource.
        skip_final_snapshot(bool, optional): Mention this true if you want to skip creating snapshot default is false.
                                   Either this or final_db_snapshot_identifier should be provided
        final_db_snapshot_identifier(Text, optional): Identifier for the created final db_snapshot
        timeout(Dict, optional): Timeout configuration for deletion of AWS DB Cluster.
            * delete (Dict) -- Timeout configuration for deletion of a DB Cluster
                * delay -- The amount of time in seconds to wait between attempts.
                * max_attempts -- Customized timeout configuration containing delay and max attempts.

    Returns:
        Dict[str, Any]

    Examples:

        .. code-block:: sls

            test-db-cluster:
              aws.rds.db_cluster.absent:
                - resource_id: test-db-cluster,
                - skip_final_snapshot: true
    """

    result = dict(comment=(), old_state="", new_state="", name=name, result=True)

    before = await hub.exec.boto3.client.rds.describe_db_clusters(
        ctx, DBClusterIdentifier=resource_id
    )

    if not before["ret"]:
        result["comment"] = (f"aws.rds.db_cluster '{name}' already absent",)

    else:
        result[
            "old_state"
        ] = hub.tool.aws.rds.conversion_utils.convert_raw_db_cluster_to_present(
            raw_resource=before["ret"]["DBClusters"][0], idem_resource_name=name
        )
        if ctx.get("test", False):
            result["comment"] = (f"Would delete aws.rds.db_cluster '{name}'",)
            return result
        try:
            ret = await hub.exec.boto3.client.rds.delete_db_cluster(
                ctx,
                DBClusterIdentifier=resource_id,
                SkipFinalSnapshot=skip_final_snapshot,
                FinalDBSnapshotIdentifier=final_db_snapshot_identifier,
            )
            result["result"] = ret["result"]
            if not result["result"]:
                result["comment"] = ret["comment"]
                result["result"] = False
                return result
            # Custom waiter for delete
            waiter_config = hub.tool.aws.waiter_utils.create_waiter_config(
                default_delay=15,
                default_max_attempts=40,
                timeout_config=timeout.get("delete") if timeout else None,
            )
            cluster_waiter = hub.tool.boto3.custom_waiter.waiter_wrapper(
                name="ClusterDelete",
                operation="DescribeDBClusters",
                argument=["Error.Code", "DBClusters[].Status"],
                acceptors=delete_waiter_acceptors,
                client=hub.tool.boto3.client.get_client(ctx, "rds"),
            )
            await hub.tool.boto3.client.wait(
                ctx,
                "rds",
                "ClusterDelete",
                cluster_waiter,
                DBClusterIdentifier=resource_id,
                WaiterConfig=waiter_config,
            )
            result["comment"] = (f"Deleted aws.rds.db_cluster '{name}'",)
        except hub.tool.boto3.exception.ClientError as e:
            result["comment"] = result["comment"] + (f"{e.__class__.__name__}: {e}",)

    return result


async def describe(hub, ctx) -> Dict[str, Dict[str, Any]]:
    r"""
    **Autogenerated function**

    Describe the resource in a way that can be recreated/managed with the corresponding "present" function


    Returns information about Amazon Aurora DB clusters and Multi-AZ DB clusters. This API supports pagination. For
    more information on Amazon Aurora DB clusters, see  What is Amazon Aurora? in the Amazon Aurora User Guide.  For
    more information on Multi-AZ DB clusters, see  Multi-AZ deployments with two readable standby DB instances in
    the Amazon RDS User Guide.   The Multi-AZ DB clusters feature is in preview and is subject to change.  This
    operation can also return information for Amazon Neptune DB instances and Amazon DocumentDB instances.


    Returns:
        Dict[str, Any]

    Examples:

        .. code-block:: bash

            $ idem describe aws.rds.db_cluster
    """

    result = {}
    ret = await hub.exec.boto3.client.rds.describe_db_clusters(ctx)
    if not ret["result"]:
        hub.log.debug(f"Could not describe db_cluster {ret['comment']}")
        return {}

    for db_cluster in ret["ret"]["DBClusters"]:
        # Including fields to match the 'present' function parameters
        resource_id = db_cluster.get("DBClusterIdentifier")
        resource_translated = (
            hub.tool.aws.rds.conversion_utils.convert_raw_db_cluster_to_present(
                raw_resource=db_cluster, idem_resource_name=resource_id
            )
        )
        result[resource_id] = {
            "aws.rds.db_cluster.present": [
                {parameter_key: parameter_value}
                for parameter_key, parameter_value in resource_translated.items()
            ]
        }

    return result
