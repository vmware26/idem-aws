def test_get_hosted_zone_with_filters(hub):
    hosted_zones = {
        "HostedZones": [
            {
                "Id": "/hostedzone/ABCDEF1234",
                "Name": "idem-fixture-test.com.",
                "CallerReference": "a35af6a-5afg4",
                "Config": {"Comment": "test", "PrivateZone": False},
                "ResourceRecordSetCount": 2,
                "DelegationSet": {
                    "NameServers": [
                        "ns-696.awsdns-23.net",
                        "ns-1669.awsdns-16.co.uk",
                        "ns-1319.awsdns-36.org",
                        "ns-209.awsdns-26.com",
                    ]
                },
                "Tags": [{"Key": "test", "Value": "value"}],
            },
            {
                "Id": "/hostedzone/ABCDEF56789",
                "Name": "example.com.",
                "CallerReference": "sg5aga-af42fa",
                "Config": {"Comment": "", "PrivateZone": True},
                "ResourceRecordSetCount": 2,
                "VPCs": [
                    {"VPCRegion": "eu-west-3", "VPCId": "vpc-123a"},
                    {"VPCRegion": "eu-west-3", "VPCId": "vpc-123b"},
                    {"VPCRegion": "eu-west-3", "VPCId": "vpc-123c"},
                ],
            },
        ]
    }
    # Filtering a hosted_zone that is present in the above example Dict.
    ret = hub.tool.aws.route53.hosted_zone_utils.get_hosted_zone_with_filters(
        raw_hosted_zones=hosted_zones,
        hosted_zone_name="idem-fixture-test.com.",
        private_zone=False,
        vpc_id=None,
        tags=None,
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]["ret"]["HostedZone"] in hosted_zones["HostedZones"]

    # Try filtering hosted_zone not present in the example Dict.
    ret = hub.tool.aws.route53.hosted_zone_utils.get_hosted_zone_with_filters(
        raw_hosted_zones=hosted_zones,
        hosted_zone_name="example.com.",
        private_zone=True,
        vpc_id="vpc-123d",
        tags=None,
    )
    assert ret["result"], ret["comment"]
    assert (
        f"Unable to find aws.route53.hosted_zone resource with given filters"
        in ret["comment"]
    )
    assert not ret["ret"]
