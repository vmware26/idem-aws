import copy
import uuid
from collections import ChainMap

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
async def test_permission(hub, ctx, aws_lambda_function, aws_iam_role_2):
    name = "test-statement" + str(uuid.uuid4())
    action = "lambda:GetAlias"
    function_name = aws_lambda_function["name"]
    principal = aws_iam_role_2["arn"]

    test_ctx = copy.deepcopy(ctx)
    test_ctx["test"] = True

    # Idem state --test
    response = await hub.states.aws.lambda_aws.function_permission.present(
        test_ctx,
        action=action,
        principal=principal,
        name=name,
        function_name=function_name,
    )

    assert response["result"], response["comment"]
    assert (
        f"Would create aws.lambda_aws.function_permission '{name}'"
        in response["comment"]
    )
    assert not response.get("old_state") and response.get("new_state")
    resource = response.get("new_state")
    assert name == resource.get("name")
    assert function_name == resource.get("function_name")
    assert action == resource.get("action")
    assert principal == resource["principal"].get("AWS")
    # Idem state real
    response = await hub.states.aws.lambda_aws.function_permission.present(
        ctx,
        action=action,
        principal=principal,
        name=name,
        function_name=function_name,
    )

    assert response["result"], response["comment"]
    assert not response.get("old_state") and response.get("new_state")
    resource = response.get("new_state")
    assert name == resource.get("name")
    assert name == resource.get("resource_id")
    assert function_name == resource.get("function_name")
    assert action == resource.get("action")
    if hub.tool.utils.is_running_localstack(ctx):
        assert principal == resource["principal"].get("Service")
    else:
        assert principal == resource["principal"].get("AWS")
    # Describe
    describe_response = await hub.states.aws.lambda_aws.function_permission.describe(
        ctx
    )
    assert describe_response[name]
    resource = dict(
        ChainMap(*describe_response[name]["aws.lambda_aws.function_permission.present"])
    )
    assert resource.get("resource_id")
    assert name == resource.get("resource_id")
    assert resource.get("action")
    assert action == resource.get("action")
    assert resource.get("principal")
    if hub.tool.utils.is_running_localstack(ctx):
        assert principal == resource["principal"].get("Service")
    else:
        assert principal == resource["principal"].get("AWS")
    assert resource.get("effect")
    assert resource.get("name")
    assert name == resource.get("name")
    assert resource.get("function_name")
    assert function_name == resource.get("function_name")
    assert aws_lambda_function.get("function_arn") == resource.get("source_arn")

    # Delete test context
    delete_response = await hub.states.aws.lambda_aws.function_permission.absent(
        test_ctx,
        name=name,
        resource_id=name,
        function_name=function_name,
    )
    assert delete_response["result"], delete_response["comment"]
    assert delete_response.get("old_state") and not delete_response.get("new_state")
    assert (
        f"Would delete aws.lambda_aws.function_permission '{name}'"
        in delete_response["comment"]
    )

    # Delete real
    delete_response = await hub.states.aws.lambda_aws.function_permission.absent(
        ctx,
        name=name,
        function_name=function_name,
    )
    assert delete_response["result"], delete_response["comment"]
    assert f"Deleted aws.lambda_aws.function_permission '{name}'"
    assert delete_response.get("old_state") and not delete_response.get("new_state")
    resource = delete_response.get("old_state")
    assert name == resource.get("name")
    assert name == resource.get("resource_id")
    assert action == resource.get("action")
    if hub.tool.utils.is_running_localstack(ctx):
        assert principal == resource["principal"].get("Service")
    else:
        assert principal == resource["principal"].get("AWS")
    assert function_name == resource.get("function_name")
    # Trying to delete an already deleted resource. It should say resource already got deleted.
    ret = await hub.states.aws.lambda_aws.function_permission.absent(
        ctx,
        name=name,
        resource_id=name,
        function_name=function_name,
    )
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        f"aws.lambda_aws.function_permission '{name}' already absent" in ret["comment"]
    )
